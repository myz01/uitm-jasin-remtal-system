package rentalDAO;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import rental.model.student;
import rental.connection.ConnectionManager;

public class StudentDAO {

	private static Connection con = null;
	private static PreparedStatement ps = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private static int id;
	private static String name;
	private static String email;
	private static String password;
	private static String phone;
	private static boolean valid;
	private static String sql;
	
	public static student login(student bean) throws NoSuchAlgorithmException{
		// retrieve id and password from loginController;
		id = bean.getId();
		password = bean.getPassword();
		
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(password.getBytes());
	    byte byteData[] = md.digest();

	    //convert byte to hex format
	    StringBuffer sb = new StringBuffer();
	    for(int i = 0; i < byteData.length; i++) {
	      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,16).substring(1));
	    }
		
	 try {   
		//call getConnection() method
			con = ConnectionManager.getConnection();
		 
			//3. create statement
		    sql = "select * from student where StudID = '"+id +"'and StudPass ='"+sb.toString()+"' ";  
			stmt = con.createStatement();
		 
			//4. execute query
			rs = stmt.executeQuery(sql);
			boolean more = rs.next();

			if(more) {
				bean.setId(rs.getInt("StudID"));
				bean.setName(rs.getString("StudName"));
				bean.setEmail(rs.getString("StudEmail"));
				bean.setPhone(rs.getString("StudPhone"));
				bean.setValid(true);
				
			}else if(!more)
			{
				bean.setValid(false);
			}
			//5. close connection
			con.close();
			
	 }catch(Exception e) {
		e.printStackTrace();				
	}
		
		
		
		return bean;
	}
	
	public void addStudent(student bean) throws NoSuchAlgorithmException {

		id = bean.getId();
		name = bean.getName();
		password = bean.getPassword();
		email = bean.getEmail();
		phone = bean.getPhone();
		
		 MessageDigest md = MessageDigest.getInstance("MD5");
		 md.update(password.getBytes());
		 byte byteData[] = md.digest();

		    //convert byte to hex format
		 StringBuffer sb = new StringBuffer();
		 for(int i = 0; i < byteData.length; i++) {
		  sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,16).substring(1));
		    }
		
		try {			
			//call getConnection() method
			con = ConnectionManager.getConnection();

			//3. create statement
		 	sql = "INSERT INTO student(studid,studname,studpass,studemail,studphone)VALUES(?,?,?,?,?)";
			ps = con.prepareStatement(sql);				
			ps.setInt(1, id);
			ps.setString(2, name);
			ps.setString(3,sb.toString());
			ps.setString(4,email);
			ps.setString(5, phone);

			//4. execute query
			ps.executeUpdate();

			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();				
		}
	}	
	
	
	
	
	public static student getUserById(int userid) {
		student user = new student();
		try {
			//call getConnection() method
			con = ConnectionManager.getConnection();

			//3. create statement 
			ps = con.prepareStatement("SELECT * FROM student WHERE studid=?");
			ps.setInt(1, userid);

			//4. execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				user.setId(rs.getInt("studid"));
				user.setName(rs.getString("studname"));
				user.setEmail(rs.getString("studemail"));
				user.setPhone(rs.getString("studphone"));		
			}
			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();	
		}			
		return user;
	}
	
	
	
}
