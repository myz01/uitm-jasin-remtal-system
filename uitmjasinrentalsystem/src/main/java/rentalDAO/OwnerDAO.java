package rentalDAO;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import rental.connection.ConnectionManager;
import rental.model.homeowner;
import rental.model.student;

public class OwnerDAO {
	private static Connection con = null;
	private static PreparedStatement ps = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private static int id;
	private static String name;
	private static String pass;
	private static String number;
	private static String email;
	private static boolean valid;
	private static String sql;
	
	public static homeowner login(homeowner bean) throws NoSuchAlgorithmException{
		// retrieve id and password from loginController;
		id = bean.getId();
		pass = bean.getPass();
		
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(pass.getBytes());
	    byte byteData[] = md.digest();

	    //convert byte to hex format
	    StringBuffer sb = new StringBuffer();
	    for(int i = 0; i < byteData.length; i++) {
	      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,16).substring(1));
	    }
		
	 try {   
		//call getConnection() method
			con = ConnectionManager.getConnection();
		 
			//3. create statement
		    sql = "select * from homeowner where OwnerID = '"+id +"'and OwnerPass ='"+sb.toString()+"' ";  
			stmt = con.createStatement();
		 
			//4. execute query
			rs = stmt.executeQuery(sql);
			boolean more = rs.next();

			if(more) {
				bean.setId(rs.getInt("OwnerID"));
				bean.setName(rs.getString("OwnerName"));
				bean.setEmail(rs.getString("OwnerEmail"));
				bean.setNumber(rs.getString("OwnerNumber"));
				bean.setValid(true);
				
			}else if(!more)
			{
				bean.setValid(false);
			}
			//5. close connection
			con.close();
			
	 }catch(Exception e) {
		e.printStackTrace();				
	}
		return bean;
	}
	
	public static homeowner getUserById(int userid) {
		homeowner user = new homeowner();
		try {
			//call getConnection() method
			con = ConnectionManager.getConnection();

			//3. create statement 
			ps = con.prepareStatement("SELECT * FROM homeowner WHERE ownerid=?");
			ps.setInt(1, userid);

			//4. execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				user.setId(rs.getInt("ownerid"));
				user.setName(rs.getString("ownername"));
				user.setNumber(rs.getString("ownernumber"));
				user.setEmail(rs.getString("owneremail"));		
			}
			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();	
		}			
		return user;
	}
	
	
}
