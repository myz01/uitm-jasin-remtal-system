package rental.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import rental.model.student;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import rentalDAO.StudentDAO;

/**
 * Servlet implementation class InsertController
 */
public class InsertControllerS extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private StudentDAO dao;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertControllerS() {
        super();
        dao = new StudentDAO();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//create user object
				student user = new student();
				
				//retrieve from HTML and set the values
				user.setId(Integer.parseInt(request.getParameter("id")));
				user.setName(request.getParameter("name"));
				user.setPassword(request.getParameter("password"));
				user.setEmail(request.getParameter("email"));
				user.setPhone(request.getParameter("phone"));	
				
				//invoke method addUser() in UserDAO
					
						dao.addStudent(user);
					
				//set attribute to a servlet request
//				request.setAttribute("student", StudentDAO.getAllUsers()); 
				
				//forward the request to listUsers.jsp
				
			   RequestDispatcher view = request.getRequestDispatcher("login.html");
				view.forward(request, response);
	}

}
