package rental.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import rentalDAO.StudentDAO;
import rentalDAO.OwnerDAO;
import rental.model.student;
import rental.model.homeowner;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private StudentDAO dao;
    private HttpSession session;
    private RequestDispatcher view;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        dao = new StudentDAO();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
		String option = request.getParameter("option");

		
		if(option.equalsIgnoreCase("student"))
		{
		student user = new student();
		int id = Integer.parseInt(request.getParameter("id"));
		user.setId(id);	
		user.setPassword(request.getParameter("password"));
		
		user = StudentDAO.login(user);
		if(user.isValid())
		{
			session = request.getSession(true);
			session.setAttribute("sessionId", user.getId());
			session.setAttribute("sessionName", user.getName());
			session.setAttribute("sessionEmail", user.getEmail());
			session.setAttribute("sessionPhone", user.getPhone());
			
			request.setAttribute("student",StudentDAO.getUserById(id));
			view = request.getRequestDispatcher("homeStudent.jsp");
		    view.forward(request,response);
			
		}
		else {
			response.sendRedirect("invalidLogin.jsp");
		}
		}
		else if(option.equalsIgnoreCase("owner"));
		{
			homeowner user = new homeowner();
			int id = Integer.parseInt(request.getParameter("id"));
			user.setId(id);
			user.setPass(request.getParameter("password"));
			
			user = OwnerDAO.login(user);
			if(user.isValid())
			{
				session = request.getSession(true);
				session.setAttribute("sessionId", user.getId());
				session.setAttribute("sessionName", user.getName());
				session.setAttribute("sessionEmail", user.getEmail());
				session.setAttribute("sessionPhone", user.getNumber());
				
				request.setAttribute("owner",OwnerDAO.getUserById(id));
				view = request.getRequestDispatcher("homeHouseOwner.jsp");
			    view.forward(request,response);
				
			}
			else {
				response.sendRedirect("invalidLogin.jsp");
			}
		}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
