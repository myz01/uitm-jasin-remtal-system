<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%  
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);
  
  session = request.getSession();
  
  if(session.getAttribute("sessionEmail")==null)	  
      response.sendRedirect("login.jsp");  
  %>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  <!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v5.7.0, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v5.7.0, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/logouitm-96x41.png" type="image/x-icon">
  <meta name="description" content="">
  
  
  <title>PROFILE SITE STUDENT</title>
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/animatecss/animate.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="preload" href="https://fonts.googleapis.com/css?family=Inter:100,200,300,400,500,600,700,800,900&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:100,200,300,400,500,600,700,800,900&display=swap"></noscript>
  <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

  <%
    
	String email = (String)session.getAttribute("sessionEmail");
	String name = (String)session.getAttribute("sessionName");
	int id = (int)session.getAttribute("sessionId");
	String phone = (String)session.getAttribute("sessionPhone");
	
	%>
  
  
</head>
<body>
  
  <section data-bs-version="5.1" class="menu menu2 cid-ts2kzzSXRs" once="menu" id="menu2-2i">
    
    <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg bg-light">
        <div class="container">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobiri.se">
                        <img src="assets/images/logouitm-96x41.png" alt="Mobirise Website Builder" style="height: 3rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-black text-primary display-7" href="index.html">UITM JASIN HOUSE RENTAL SYSTEM</a></span>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-bs-toggle="collapse" data-target="#navbarSupportedContent" data-bs-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true"><li class="nav-item dropdown open"><a class="nav-link link text-black dropdown-toggle show display-4" href="https://mobiri.se" data-toggle="dropdown-submenu" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="true"><%= name %></a><div class="dropdown-menu show" aria-labelledby="dropdown-666" data-bs-popper="none"><a class="text-black dropdown-item display-4" href="viewProfileS.jsp">PROFILE</a><a class="text-black dropdown-item text-primary display-4" href="LogoutController">LOGOUT</a></div></li></ul>
                
                
            </div>
        </div>
    </nav>
</section>

<section data-bs-version="5.1" class="testimonials1 cid-tpxBPN8LA1" id="testimonials1-1q">
    

	
	<h2> Welcome <b> <%= name %> </b> </h2>
    
    
    <div class="container">
        
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="image-wrapper"> 
                    <img src="assets/images/peopleicon3.png" alt="Mobirise Website Builder" >
                </div> 
            </div>
            <div class="col-12 col-md">
                <div class="text-wrapper">
                    <p class="mbr-text mbr-fonts-style mb-4 display-7">STUDENT PROFILE</p>
                    <p class="name mbr-fonts-style mb-1 display-4">
<!--                        right here -->
      	
      	  	<label for="id">Student ID</label>: <%= id %><br>
    	    <label for="name">Name</label>:   	<%= name %><br>
    	    <label for="email">Email</label>: <%= email %><br>
      	    <label for="phone">Phone</label>:   <%= phone %><br>
                    </p>
<%--             <c:out value="${student.id}"/><br>        --%>
<%--              <c:out value="${student.name}"/><br>  --%>
<%--             <c:out value="${student.email}"/><br>	 --%>
<%--             <c:out value="${student.phone}"/><br> --%>
                </div>
            </div>
        </div>
    </div>
</section><section class="display-7" style="padding: 0;align-items: center;justify-content: center;flex-wrap: wrap;    align-content: center;display: flex;position: relative;height: 4rem;"><a href="https://mobiri.se/2918912" style="flex: 1 1;height: 4rem;position: absolute;width: 100%;z-index: 1;"><img alt="" style="height: 4rem;" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="></a><p style="margin: 0;text-align: center;" class="display-7">Made with &#8204;</p><a style="z-index:1" href="https://mobirise.com/builder/html-code-generator.html">No Code Website Builder</a></section><script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>  <script src="assets/smoothscroll/smooth-scroll.js"></script>  <script src="assets/ytplayer/index.js"></script>  <script src="assets/dropdown/js/navbar-dropdown.js"></script>  <script src="assets/theme/js/script.js"></script>  
  
  
 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
    <input name="animation" type="hidden">
  </body>
</html>